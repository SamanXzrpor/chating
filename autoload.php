<?php

spl_autoload_register(function($file){

    $filename = __DIR__ . "/$file.php";
    if(file_exists($filename) AND is_readable($filename)){
        include($filename);
    }
});