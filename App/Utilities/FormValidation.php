<?php
namespace App\Utilities;

include __DIR__ . '/../../vendor/autoload.php';
use Rakit\Validation\Validator;

class FormValidation 
{

    public function validate(array $data)
    {
        $validator = new Validator;

            $validation = $validator->make($data , [
                'email'                 => 'required|email',
                'password'              => 'required|min:6',
            ]);

            $validation->validate();

                if ($validation->fails()) {
                    // handling errors
                    $errors = $validation->errors();
                    echo "<pre>";
                    print_r($errors->firstOfAll());
                    echo "</pre>";
                    exit;
                }
    }

}