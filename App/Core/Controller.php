<?php
namespace App\Core;

class Controller  
{

    public function getModel(string $model)
    {
        $modelObj = 'App\Models\\' . ucfirst($model) ;

        if(class_exists($modelObj)){

            return new $modelObj;
        }else{

            return null;
        }
    }

    public function view(string $path , array $data = [])
    {
        $files = explode('.' , $path);

        $baseFile = __DIR__ .'/../Views/';

        $lastKey = array_key_last($files);

        foreach($files as $file){

            if($file == $files[$lastKey]){

                $baseFile .= $files[$lastKey] . '.php';
            }else{

                $baseFile .= $file . '/' ;
            }

            include $baseFile;
        }
    }
}