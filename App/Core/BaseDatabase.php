<?php
namespace App\Core;


use SimpleCrud\Database;
use PDOException;
use PDO;

class BaseDatabase 
{

    private $host = 'localhost';
    private $dbname = 'chating';
    private $user = 'root';
    private $pass = '';
    protected $db;
    protected $table;
    public $pdo;


    public function __construct()
    {
        try {
            $this->pdo = new PDO("mysql:host={$this->host};dbname={$this->dbname};",$this->user,$this->pass);
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
            $this->db = new Database($this->pdo);

        } catch (PDOException $e) {
            die("Database Config Error : {$e->getMessage()}");
        }
    }
    
    public function insert(array $data)
    {
        return $this->db->{$this->table}->insert($data)->get();
    }

    public function selectOne(string $con , string $value)
    {
        return $this->db->{$this->table}->select()->one()->where(''. $con.' = ', $value)->get();
        
    }

    public function selectAll(string $con , string $value ,string $orderby = 'id ASC', int $limit = 100)
    {
        return $this->db->{$this->table}->select()->where(''.$con.' = ', $value)->orderBy($orderby)->limit($limit)->get();
    }

    public function selectAll2(string $table)
    {
        $stmt = $this->pdo->prepare("SELECT * FROM $table");
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        return $result;
    }

    public function delete(int $id)
    {
        unset($this->db->{$this->table}[$id]);
    }
}