<?php
include __DIR__ . '/../../autoload.php';
class Core
{
    protected $currentRouter = [];
    protected $currentController;
    protected $currentMethod ;
    protected $currentParams = [];
    
    public function __construct()
    {
        $this->getUrl();
        $namespace = self::getControllerNamespace();
        
        if(!isset($this->currentRouter[0])){
            $homeController =new ($namespace . 'Home');
            $homeController->index();
        }

        if(isset($this->currentRouter[0])){
            $controllerName = $namespace . trim(ucfirst($this->currentRouter[0]));
            if(class_exists($controllerName)){

                $this->currentController = $controllerName;

                unset($this->currentRouter[0]);
            }else{
                die("Class Not Exist");
            }
        }

       
        if(isset($this->currentRouter[1])){

            $currentControllerObj = new $this->currentController;

            if(method_exists($currentControllerObj,$this->currentRouter[1])){

                $this->currentMethod = $this->currentRouter[1];
                
                unset($this->currentRouter[1]);
            }else{
                die("Methode Not Exist");
            }
        }else{
            return;
        }

        $this->currentParams = $this->currentRouter ? array_values($this->currentRouter) : [];

        call_user_func_array([$currentControllerObj,$this->currentMethod],$this->currentParams);
    }

    public function getUrl()
    {
        if(isset($_GET['url'])){
            $url = filter_var(rtrim($_GET['url'],'/'),FILTER_SANITIZE_STRING);
            $this->currentRouter  = explode('/',$url);
        }
    }

    public static function getControllerNamespace()
    {
        return "App\Controllers\\";
    }
}