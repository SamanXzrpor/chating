<?php
include __DIR__ . '/../../vendor/autoload.php';

use Hekmatinasser\Verta\Verta;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../public/css/chat/style.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">
    <title>Document</title>
</head>
<body>
    <div class="wrapper">
        <section class="chat-area">
            <header>
                <a class="back-icon" href="<?= ($data['userInfo']->permission == 'admin') ? "{$data['url']}showUsersChat/viewUserChat" : "{$data['url']}" ?>"><i class="fas fa-arrow-left"></i></a>
                <img src="../public/img/me.jpg" alt="">
                <div class="details">
                    <span><?= ($data['userInfo']->permission == 'admin') ? $data['chatInfo']->room_name : 'Admin' ?></span>
                    <p>Active Now!</p>
                </div>
            </header>
            <div class="chat-box">
                <?php foreach($data['msg'] as $msg): ?>
                    
                <?php if($msg->user_id == $data['userInfo']->id): ?>
                <div class="chat outgoing">
                    <div class="details">
                        <div class="all">
                            <p><?= $msg->body ?></p>
                            <p class="msg-data"><?= Verta::instance($msg->created_at)->Format('%H : i') ;  ?></p>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
                <?php if($msg->user_id !== $data['userInfo']->id): ?>
                <div class="chat incoming">
                <img src="../public/img/me.jpg" alt="">
                <div class="details">
                    <div class="all">
                            <p><?=  $msg->body ?></p>
                            <p class="msg-data"><?= Verta::instance($msg->created_at)->Format('%H : i') ; ?></p>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
                <?php endforeach; ?>
            </div>
            <form action="<?= $data['url'] ?>Chat/sendMessage/<?= ($data['userInfo']->permission == 'admin') ? $data['chatInfo']->id : '' ?>" id="form" method="POST" class="typing-area">
                <input id="message" name="message" type="text" placeholder="Type a message ...">
                <button type="submit" id="sendMessage"><i class="fab fa-telegram-plane"></i></button>
            </form>
        </section>
    </div>
    <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
    <script src="<?= $data['url'] . 'public/js/chat/main.js' ?>"></script>
</body>
</html>