<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<?= ($data['currentInfo']->permission == 'user') ? "
    <div class='chat-btn'>
        <a href='{$data['url']}Chat/viewChat'><button class='button button-primary'>ChatRoom</button></a>
    </div>
    " : '';
    ?>
    <?= ($data['currentInfo']->permission == 'admin') ? "
    <div class='panel'>
    <a href='{$data['url']}ShowUsersChat/viewUserChat'><button class='button button-primary'>Dashboard</button></a>
    </div>
    " : '';
    ?>
</body>
</html>