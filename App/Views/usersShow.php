<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../public/css/chat/style.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">
    <title>Document</title>
</head>
<body>
    <div class="wrapper">
        <section class="users">
            <header>
                <div class="content">
                    <img src="../public/img/me.jpg" alt="">
                    <div class="details">
                        <span><?= $data['currentInfo']->name ?></span>
                        <p>Active Now!</p>
                    </div>
                </div>
                <a href="<?= $data['url'] ?>Home/index" class="logout">LogOut</a>
            </header>
            <div class="search">
                <span class="text">Select Use For Strart Chat!</span>
                <input type="text" placeholder="Inter Name To search">
                <button><i class="fas fa-search"></i></button>
            </div>
            <div class="users-list">
                <?php foreach ($data['chats'] as $chat): ?>
                <a href="<?= $data['url'] ?>Chat/viewChat?chatID=<?= $chat->id ?>">
                    <div class="content">
                        <div class="details">
                            <span><?= $chat->room_name ?></span>
                            <p>This Is Test Message</p>
                        </div>
                    </div>
                    <div class="status-dot"><i class="fas fa-circle"></i></div>
                </a>
                <?php endforeach; ?>
            </div>
        </section>
    </div>
</body>
</html>