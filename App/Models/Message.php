<?php
namespace App\Models;

use App\Core\BaseDatabase;

class Message extends BaseDatabase
{
    protected $table = 'message';
}