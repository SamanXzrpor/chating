<?php
namespace App\Helpers;

class Helpers 
{

    # app.url or database.host
    public static function config(string $path)
    {
        $args     = explode('.' , $path);

        $file     = $args[0];

        $keyArray = $args[1];

        $basePath = __DIR__ . '/../Config/';

        $baseFile = include $basePath . $file .'.php';

        return $baseFile[$keyArray];
    }

    public static function redirect(string $path)
    {
        header('Location: '. self::config('app.url') . $path);
    }

    public static function notif(string $notif)
    {
        
        return "<div style='position: absolute;
        color: #fbfbfb;
        border-radius: 6px;
        padding: 12px 20px;
        top: 22px;
        left: 566px;
        font-size: larger;'>$notif</div>";
    }

}