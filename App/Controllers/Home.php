<?php
namespace App\Controllers;

use App\Core\Controller;
use App\Helpers\Helpers;
use App\Controllers\Login;

class Home extends Controller 
{

    private $chatModel;
    private $msgModel;
    private $userModel;

    public function __construct()
    {
        
        if(!Login::isLogin()){
            Helpers::redirect('Login/viewLogin');
        }
        
        $this->chatModel = $this->getModel('Chats');
        $this->msgModel = $this->getModel('message');
        $this->userModel = $this->getModel('Users');
    }

    public function index() 
    {
        $data = ['url'=>Helpers::config('app.url'),'currentInfo'=>$this->getCurrentUserInfo()];

        $this->view('home',$data);
    }

    public function getCurrentUserInfo()
    {
        return $this->userModel->selectOne('id',Login::currentUserID());
    }

}