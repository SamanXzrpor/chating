<?php
namespace App\Controllers;


use App\Core\Controller;
use App\Helpers\Helpers;
use App\Utilities\FormValidation;
class Register extends Controller 
{
    private $notif = '';

    public function viewRegister()
    {

        $data = ['url' => Helpers::config('app.url'),'notif'=>Helpers::notif($this->notif)];

        $this->view('register',$data);
    }

    public function register()
    {
        $userModel =$this->getModel('Users');
        $formValidator = new FormValidation();

        if($_SERVER['REQUEST_METHOD'] == 'POST'){

            $formValidator->validate($_POST);

            $userModel->insert(['name'=>$_POST['name'],'email'=>$_POST['email'],
            'password'=>password_hash($_POST['password'],PASSWORD_BCRYPT),'number'=>$_POST['number']]);

            Helpers::redirect('Login/viewLogin');
        
        }
    }
}