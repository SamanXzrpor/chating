<?php
namespace App\Controllers;

use App\Core\Controller;
use App\Helpers\Helpers;
use App\Utilities\FormValidation;

class Login extends Controller
{

    public function viewLogin()
    {
        $data = ['url'=>Helpers::config('app.url')];
        $this->view('login',$data);
    }

    public function getInfoUser(string $email)
    {
        $userModel = $this->getModel('Users');

        return $userModel->selectOne('email',$email);

    }

    public function login()
    {
        $formValidator = new FormValidation();

        if($_SERVER['REQUEST_METHOD'] == 'POST'){

            $formValidator->validate($_POST);

            $infos = $this->getInfoUser($_POST['email']);

            if($_POST['email'] == $infos->email){

                if(password_verify($_POST['password'], $infos->password)){

                    $_SESSION['userID'] = $infos->id;

                    Helpers::redirect('');
                }else{
                    Helpers::redirect('Login/viewLogin');
                }
            }else{
                Helpers::redirect('Login/viewLogin');
            }

        }
    }

    public static function currentUserID()
    {
        return $_SESSION['userID'];
    }
    public function adminInfo()
    {
        $userModel = $this->getModel('Users');

        return $userModel->selectAll('permission','admin');
    }

    public static function isLogin()
    {
        return isset($_SESSION['userID']) ? true : false;
    }
}

