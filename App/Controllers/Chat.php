<?php
namespace App\Controllers;

include __DIR__ . '/../../vendor/autoload.php';

use Hekmatinasser\Verta\Verta;
use App\Core\Controller;
use App\Helpers\Helpers;
use App\Controllers\Login;
use PDO;

class Chat extends Controller 
{

    private $chatModel;
    private $msgModel;
    private $userModel;
    private $currentChatIDForAdmin ;

    public function __construct()
    {
        $this->chatModel = $this->getModel('Chats');
        $this->msgModel = $this->getModel('message');
        $this->userModel = $this->getModel('Users');
    }

    public function viewChat() 
    {

        $verta = new Verta();

        if(isset($_GET['chatID'])){

            $this->currentChatIDForAdmin = $_GET['chatID'];
        }



        $userInfo = $this->userModel->selectOne('id',Login::currentUserID());
        $userModel = $this->getModel('Users');


        if(!Login::isLogin()){

            Helpers::redirect('Login/viewLogin');
        }



        if($this->chatByUserID(Login::currentUserID())  == null AND $this->getCurrentUserInfo()->permission !== 'admin'){

            $this->chatModel->insert(['room_name'=>$userInfo->name,'user_id'=>Login::currentUserID()]);
        }


        
        $info = $this->getCurrentUserInfo();

        $this->view('chat',['chatInfo'=> $this->chatByID($this->currentChatIDForAdmin),'userInfo'=>$info,'url'=>Helpers::config('app.url'),
         'msg'=> $this->msg()]);
    }
    


    public function chatByID($chatID = null){

        if(isset($chatID)){

            return $this->chatModel->selectOne('id',$chatID);
        }

    }



    public function chatByUserID($userID = null)
    {
        if(isset($userID)){

            return $this->chatModel->selectOne('user_id',$userID);
        }
    }




    public function sendMessage(int $chatId = null)
    {
        if($_SERVER['REQUEST_METHOD'] == 'POST'){

            if($this->getCurrentUserInfo()->permission !== 'admin')
            {
                $chat_id = $this->chatByUserID(Login::currentUserID())->id;

                $this->msgModel->insert(['body'=>$_POST['message'],'user_id'=>Login::currentUserID()
                ,'chat_id'=> $chat_id]);
            }else{
 
                if(isset($chatId)){

                    $this->msgModel->insert(['body'=>$_POST['message'],'user_id'=>Login::currentUserID()
                    ,'chat_id'=> $chatId]);
                }
            }

        }
    }



    public function msg()
    {
        $msgModel = $this->getModel('message');
        if($this->getCurrentUserInfo()->permission == 'admin'){

            $chatID = $this->chatByID($this->currentChatIDForAdmin)->id;

        }else{
            $chatID = $this->chatByUserID(Login::currentUserID())->id ;
        }

        return $msgModel->selectAll('chat_id',$chatID);
    }




    public function getCurrentUserInfo()
    {
        return $this->userModel->selectOne('id',Login::currentUserID());
    }
}

