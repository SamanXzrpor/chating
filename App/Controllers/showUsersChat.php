<?php
namespace App\Controllers;

use App\Core\Controller;
use App\Helpers\Helpers;

class showUsersChat extends Controller 
{

    private $chatModel;
    private $msgModel;
    private $userModel;

    public function __construct()
    {
        $this->chatModel = $this->getModel('Chats');
        $this->msgModel = $this->getModel('message');
        $this->userModel = $this->getModel('Users');
    }

    public function viewUserChat() 
    {
        $data = ['chats'=>$this->getChats(),'currentInfo' => $this->getCurrentUserInfo(),
        'url'=>Helpers::config('app.url')];
        
        $this->view('usersShow',$data);
    }

    public function getChats() 
    {
        return $this->chatModel->selectAll2('chatroom');
    }

    public function getCurrentUserInfo()
    {
        return $this->userModel->selectOne('id',Login::currentUserID());
    }
}