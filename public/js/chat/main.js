
$(document).ready(function() {

    $("#form").on("submit", function(event){

        event.preventDefault();

        $.ajax({
            url    : $("#form").attr('action'),
            method : 'POST',
            data   :{action : 'sendMessage', message:$("#message").val()},
            success: function(data) {
                window.top.location = window.top.location ;
            },
            error: function(error) {
                alert(error);
            }
        })
    })
})